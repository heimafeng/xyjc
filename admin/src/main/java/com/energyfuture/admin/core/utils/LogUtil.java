package com.energyfuture.admin.core.utils;

import com.energyfuture.admin.system.domain.ActionLog;
import com.energyfuture.admin.system.service.ActionLogService;
import com.energyfuture.core.utils.ReflexBeanUtil;
import com.energyfuture.core.utils.SpringContextUtil;

import javax.persistence.Table;
import java.lang.reflect.InvocationTargetException;
import java.util.List;


/**
 * @author 小懒虫
 * @date 2018/10/16
 */
public class LogUtil {

    /**
     * 获取实体对象的日志
     * @param entity 实体对象
     */
    public static List<ActionLog> entityList(Object entity){
        ActionLogService actionLogService = SpringContextUtil.getBean(ActionLogService.class);
        Table table = entity.getClass().getAnnotation(Table.class);
        String tableName = table.name();
        try {
            Object object = ReflexBeanUtil.getField(entity, "id");
            Long entityId = Long.valueOf(String.valueOf(object));
            return actionLogService.getDataLogList(tableName, entityId);
        } catch (InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
