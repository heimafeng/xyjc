package com.energyfuture.admin.core.utils;

import com.energyfuture.admin.system.domain.Tree;
import org.apache.poi.ss.formula.functions.T;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * @Auther: likai
 * @Date: 2018 2018/8/4 0004 18:37
 * @Description:组装多级树帮助类
 */
public class PackageTree {

    /**
     *
     *
     * @param: treeTable 为表数据，name,code,child分别为输出的树节点的名称
     * name  显示名称
     * code  id
     * child  子集合名称
     * @return:
     * @auther: likai
     * @description:
     * @date: 2018/8/5 0005 15:33
     */
    public static List<Map<String,Object>> pkTree(List<Map<String,Object>> treeTable,
                                                  String inName,String inCode,
                                                  String inParent,String inJb,
                                                  String inOtherData,String outName,
                                                  String outCode,String outChild,
                                                  String outJb,String outOtherData){
        Tree tree = new Tree();
        tree.setInName(inName);
        tree.setInCode(inCode);
        tree.setInParent(inParent);
        tree.setInJb(inJb);
        tree.setInOtherData(inOtherData);
        tree.setOutName(outName);
        tree.setOutCode(outCode);
        tree.setOutChild(outChild);
        tree.setOutJb(outJb);
        tree.setOutOtherData(outOtherData);
        List<Map<String,Object>> treeData = new ArrayList<>();
        List<Map<String,Object>> parentTreeData = queryParent(tree, treeTable);
        for(int i=0;i<parentTreeData.size();i++){
            Map<String,Object> parentMap = parentTreeData.get(i);
            parentMap = recursionTree(tree, treeTable, parentMap);
            treeData.add(parentMap);
        }
        //删除多余层级
        //子菜单ids
        String[] ids = searchChild(treeData, tree, "").split(",");

        Iterator<Map<String, Object>> it = treeData.iterator();
        while(it.hasNext()) {
            Map<String, Object> map = it.next();
            for (String s : ids) {
                if (String.valueOf(map.get("id")).equals(s)) {
                    it.remove();
                }
            }
        }
        return treeData;
    }


    /**
     *
     *
     * @param:
     * @return:
     * @auther: likai
     * @description: 找到最父级
     * @date: 2018/8/4 0004 19:26
     */
    public static List<Map<String,Object>> queryParent(Tree tree, List<Map<String,Object>> treeTable){
        List<Map<String,Object>> parentMapList = new ArrayList<>();
        try{
            for( int i=0;i<treeTable.size();i++){
                Map<String,Object> nodeMap = treeTable.get(i);
                String code = nodeMap.get(tree.getInCode())==null?null:nodeMap.get(tree.getInCode()).toString();
                String name = nodeMap.get(tree.getInName())==null?null:nodeMap.get(tree.getInName()).toString();
                String jb = nodeMap.get(tree.getInJb())==null?null:nodeMap.get(tree.getInJb()).toString();
                String[] other = tree.getInOtherData().split(",");  //其他值
                Object otherData = null;

                List<Map<String,Object>> childMapList = new ArrayList<>();
                Map<String,Object> pkMap = new HashMap<>();
                pkMap.put(tree.getOutChild(),childMapList);
                pkMap.put(tree.getOutName(),name);
                pkMap.put(tree.getOutJb(),jb);
                pkMap.put(tree.getOutCode(),code);
                for(String s : other){
                    otherData = nodeMap.get(s)==null?null:nodeMap.get(s);
                    pkMap.put(s, otherData);
                }
                parentMapList.add(pkMap);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return parentMapList;
    }

    /**
     * 寻找被冻结节点是否存在
     * @return
     */
    private static String searchChild(List<Map<String, Object>> list, Tree tree, String ids){
        for(Map<String, Object> map : list){
            List<Map<String, Object>> list2 = (List) map.get(tree.getOutChild());
            if(list2 != null && list2.size() > 0){
                for(Map<String, Object> map2 : list2){
                    ids += String.valueOf(map2.get("id")) + ",";
                }
                searchChild(list2, tree ,ids);
            }
        }
        return ids;
    }
    /**
     *
     *
     * @param:
     * @return:
     * @auther: likai
     * @description: 递归组装树
     * @date: 2018/8/5 0005 14:39
     */
    public static Map<String,Object> recursionTree(Tree tree, List<Map<String,Object>> treeTable, Map<String,Object> treeData){
        String parentId = treeData.get(tree.getOutCode())==null?null:treeData.get(tree.getOutCode()).toString();//获取父节点的code
        List<Map<String,Object>> childMapList = (List<Map<String,Object>>)treeData.get(tree.getOutChild());//获取到父节点的子容器
        Map<String,Object> childMap ;
        try{
            for(int i=0;i<treeTable.size();i++){
                childMap = treeTable.get(i);
                String parent = childMap.get(tree.getInParent())==null?null:childMap.get(tree.getInParent()).toString();
                String inJb = childMap.get(tree.getInJb())==null?null:childMap.get(tree.getInJb()).toString();
                if(null!=parent&&parent.equals(parentId)){//如果找到了子节点
                    List<Map<String,Object>> newChildList = new ArrayList<>();
                    childMap.put(tree.getOutChild(),newChildList);
                    String name = childMap.get(tree.getInName()).toString();
                    String code = childMap.get(tree.getInCode()).toString();
                    String jb = childMap.get(tree.getInJb())==null?null:childMap.get(tree.getInJb()).toString();
                    Object otherData = null;
                    String[] other = tree.getInOtherData().split(",");  //其他值
                    Map<String,Object> pkMap = new HashMap<>();
                    pkMap.put(tree.getOutName(),name);
                    pkMap.put(tree.getOutCode(),code);
                    pkMap.put(tree.getOutJb(),jb);
                    for(String s : other){
                        otherData = childMap.get(s)==null?null:childMap.get(s);
                        pkMap.put(s, otherData);
                    }
                    pkMap.put(tree.getOutChild(),newChildList);
                    childMapList.add(pkMap);
                    treeData.put(tree.getOutChild(),childMapList);
                    recursionTree(tree, treeTable, pkMap);
                }
            }
        }catch (Exception e){

            e.printStackTrace();
        }

        if(childMapList.size()==0){//如果该节点的子节点数量为0，则删除该节点的空的子节点容器
            treeData.remove(tree.getOutChild());
        }
        return  treeData;
    }

    /**
     * 将JavaBean转换成Map
     *
     * @param obj
     * @return
     * @throws SecurityException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public static Map beanToMap(Object obj) throws NoSuchMethodException, SecurityException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException {
        // 创建map集合
        Map map = new HashMap();
        // 获取JavaBean中所有属性
        Field[] fields = obj.getClass().getDeclaredFields();

        for (Field fie : fields) {

            // 将属性第一个字母转换成大写
            String frist = fie.getName().substring(0, 1).toUpperCase();
            // 获取属性的类型
            Class<?> type = fie.getType();
            // 封装属性的get
            String getter = "";
            if ("boolean".equals(type.getName())) {
                getter = "is" + frist + fie.getName().substring(1);
            } else {
                getter = "get" + frist + fie.getName().substring(1);
            }
            // 获取JavaBean的方法
            Method method = obj.getClass().getMethod(getter, new Class[] {});
            // 调用方法,并接收返回值
            Object objec = method.invoke(obj, new Object[] {});

            // 判断返回值不为空
            if (objec != null) {
                map.put(fie.getName(), objec);
            } else {
                map.put(fie.getName(), "");
            }
        }

        return map;
    }

    /**
     * 将Map转换为JavaBean
     *
     * @param map
     * @param obj
     * @return
     * @throws SecurityException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public static Object mapToBean(Map<String, Object> map, Object obj) throws NoSuchMethodException, SecurityException,
            IllegalAccessException, IllegalArgumentException, InvocationTargetException {

        // 获取JavaBean中的所有属性
        Field[] field = obj.getClass().getDeclaredFields();

        for (Field fi : field) {
            // 判断key值是否存在
            if (map.containsKey(fi.getName())) {
                // 获取key的value值
                String value = map.get(fi.getName()).toString();
                // 将属性的第一个字母转换为大写
                String frist = fi.getName().substring(0, 1).toUpperCase();
                // 属性封装set方法
                String setter = "set" + frist + fi.getName().substring(1);
                // 获取当前属性类型
                Class<?> type = fi.getType();
                // 获取JavaBean的方法,并设置类型
                Method method = obj.getClass().getMethod(setter, type);

                // 判断属性为double类型
                if ("java.lang.String".equals(type.getName())) {

                    // 调用当前Javabean中set方法，并传入指定类型参数
                    method.invoke(obj, value);

                } else if ("int".equals(type.getName())) {

                    method.invoke(obj, Integer.parseInt(value));

                }else if ("double".equals(type.getName())) {

                    method.invoke(obj, Double.valueOf(value));


                } else if ("char".equals(type.getName())) {

                    method.invoke(obj, value);

                }
            }
        }

        return obj;
    }

}
