package com.energyfuture.admin.system.controller;

import com.energyfuture.admin.core.web.TimoExample;
import com.energyfuture.admin.system.domain.ActionLog;
import com.energyfuture.admin.system.service.ActionLogService;
import com.energyfuture.core.utils.ResultVoUtil;
import com.energyfuture.core.vo.ResultVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/actionLog")
public class ActionLogController {

    @Autowired
    private ActionLogService actionLogService;

    /**
     * 列表页面
     */
    @GetMapping("/index")
    @ResponseBody
    @RequiresPermissions("/actionLog/index")
    public Map<String, Object> index(ActionLog actionLog){

        // 创建匹配器，进行动态查询匹配
        ExampleMatcher matcher = ExampleMatcher.matching();

        // 获取日志列表
        Example<ActionLog> example = TimoExample.of(actionLog, matcher);
        Page<ActionLog> list = actionLogService.getPageList(example);

        // 封装数据
        Map<String, Object> map = new HashMap<>();
        map.put("list",list.getContent());
        map.put("page",list);
        return map;
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    @ResponseBody
    @RequiresPermissions("/actionLog/detail")
    public Map<String, Object> toDetail(@PathVariable("id") Long id){
        ActionLog actionLog = actionLogService.getId(id);
        Map<String, Object> map = new HashMap<>();
        map.put("actionLog",actionLog);
        return map;
    }

    /**
     * 删除指定的日志
     */
    @RequestMapping("/status/delete")
    @RequiresPermissions("/actionLog/delete")
    @ResponseBody
    public ResultVo delete(
            @RequestParam(value = "ids", required = false) Long id){
        if (id != null){
            actionLogService.deleteId(id);
            return ResultVoUtil.success("删除日志成功");
        }else {
            actionLogService.emptyLog();
            return ResultVoUtil.success("清空日志成功");
        }
    }

}
