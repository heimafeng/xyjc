package com.energyfuture.admin.system.controller;

import com.energyfuture.admin.core.enums.ResultEnum;
import com.energyfuture.admin.core.enums.StatusEnum;
import com.energyfuture.admin.core.exception.ResultException;
import com.energyfuture.admin.core.log.action.SaveAction;
import com.energyfuture.admin.core.log.action.StatusAction;
import com.energyfuture.admin.core.log.annotation.ActionLog;
import com.energyfuture.admin.core.utils.LogUtil;
import com.energyfuture.admin.core.utils.PackageTree;
import com.energyfuture.admin.core.web.TimoExample;
import com.energyfuture.admin.system.domain.Dept;
import com.energyfuture.admin.system.domain.User;
import com.energyfuture.admin.system.service.DeptService;
import com.energyfuture.admin.system.validator.DeptForm;
import com.energyfuture.core.utils.FormBeanUtil;
import com.energyfuture.core.utils.ResultVoUtil;
import com.energyfuture.core.vo.ResultVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Sort;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 小懒虫
 * @date 2018/12/02
 */
@RestController
@RequestMapping("/dept")
public class DeptController {

    @Autowired
    private DeptService deptService;

    /**
     * 部门列表树
     */
    @GetMapping("/treeList")
    @RequiresPermissions("/dept/index")
    @ResponseBody
    public ResultVo treeList(Dept dept) {
        // 创建匹配器，进行动态查询匹配
        ExampleMatcher matcher = ExampleMatcher.matching().
                withMatcher("title", match -> match.contains());

        // 获取用户列表
        Example<Dept> example = TimoExample.of(dept, matcher);
        Sort sort = new Sort(Sort.Direction.ASC, "sort");
        List<Dept> list = deptService.getList(example, sort);
        List<Map<String, Object>> result = new ArrayList<>();
        //实体转换为MAP
        for (Object ob : list) {
            try {
                result.add(PackageTree.beanToMap(ob));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //组装前台树结构
        List<Map<String,Object>> treeData = PackageTree.pkTree(result,"title",
                "id","pid","sort","status,pid",
                "title","id","children","sort","status,pid");
        return ResultVoUtil.success(treeData);
    }

    /**
     * 获取排序菜单列表
     */
    @GetMapping("/sortList/{pid}/{notId}")
    @RequiresPermissions({"/dept/add", "/dept/edit"})
    @ResponseBody
    public List<Map<String, Object>> sortList(
            @PathVariable(value = "pid", required = false) Long pid,
            @PathVariable(value = "notId", required = false) Long notId){
        // 本级排序菜单列表
        notId = notId != null ? notId : (long) 0;
        List<Dept> levelDept = deptService.getPid(pid, notId);
        //添加首位选项
        Dept dept = new Dept();
        dept.setId((long) 0);
        dept.setTitle("首位");
        levelDept.add(0, dept);

        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = 0; i < levelDept.size(); i++) {
            Map<String, Object> m = new HashMap<>();
            m.put("id", i);
            m.put("label", levelDept.get(i).getTitle());
            m.put("value", levelDept.get(i).getTitle());
            list.add(m);
        }
        return list;
    }

    /**
     * 点击添加展示树
     */
    @GetMapping("/add")
    @RequiresPermissions("/dept/add")
    @ResponseBody
    public List<Map<String, Object>> toAdd() {

        List<Map<String, Object>> tree = new ArrayList<>();
        Dept d = new Dept();
        List<Map<String, Object>> list = (List<Map<String, Object>>)treeList(d).getData();

        Map<String, Object> m = new HashMap<>();
        m.put("id", 0);
        m.put("title", "顶级");
        m.put("children", list);
        m.put("expand", true);
        tree.add(m);

        return tree;
    }

    /**
     * 跳转到编辑页面
     */
    @GetMapping("/edit/{id}")
    @RequiresPermissions("/dept/edit")
    @ResponseBody
    public Map<String, Object> toEdit(@PathVariable("id") Long id) {
        Dept dept = deptService.getId(id);
        Dept pDept = deptService.getId(dept.getPid());
        Dept d = new Dept();
        List<Map<String, Object>> list = (List<Map<String, Object>>)treeList(d).getData();
        Map<String, Object> map = new HashMap<>();
        if (pDept == null) {
            Dept newDept = new Dept();
            newDept.setId((long) 0);
            newDept.setTitle("顶级");
            pDept = newDept;
        }

        List<Map<String, Object>> tree = new ArrayList<>();
        Map<String, Object> m = new HashMap<>();
        m.put("id", 0);
        m.put("title", "顶级");
        m.put("children", list);
        m.put("expand", true);
        tree.add(m);

        map.put("list", tree);
        map.put("dept", dept);
        map.put("pDept", pDept);

        return map;
    }

    /**
     * 保存添加/修改的数据
     * @param deptForm 表单验证对象
     */
    @PostMapping("/save")
    @RequiresPermissions({"/dept/add", "/dept/edit"})
    @ResponseBody
    @ActionLog(name = "部门管理", message = "部门：${title}", action = SaveAction.class)
    public ResultVo save(@Validated DeptForm deptForm) {
        if (deptForm.getId() == null) {
            // 排序为空时，添加到最后
            if(deptForm.getSort() == null){
                Integer sortMax = deptService.getSortMax(deptForm.getPid());
                deptForm.setSort(sortMax !=null ? sortMax - 1 : 0);
            }

            // 添加全部上级序号
            if (deptForm.getPid() != 0) {
                Dept pDept = deptService.getId(deptForm.getPid());
                deptForm.setPids(pDept.getPids() + ",[" + deptForm.getPid() + "]");
            } else {
                deptForm.setPids("[0]");
            }
        }

        // 将验证的数据复制给实体类
        Dept dept = new Dept();
        if (deptForm.getId() != null) {
            dept = deptService.getId(deptForm.getId());
            deptForm.setPids(dept.getPids());
        }
        FormBeanUtil.copyProperties(deptForm, dept);

        // 排序功能
        Integer sort = deptForm.getSort();
        Long notId = dept.getId() != null ? dept.getId() : 0;
        List<Dept> levelDept = deptService.getPid(deptForm.getPid(), notId);
        levelDept.add(sort, dept);
        for (int i = 1; i <= levelDept.size(); i++) {
            levelDept.get(i - 1).setSort(i);
        }

        // 保存数据
        deptService.save(levelDept);
        return ResultVoUtil.SAVE_SUCCESS;
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    @ResponseBody
    @RequiresPermissions("/dept/detail")
    public Map<String, Object> toDetail(@PathVariable("id") Long id, Model model){
        Dept dept = deptService.getId(id);
        User createBy = dept.getCreateBy();
        User updateBy = dept.getUpdateBy();
        Map<String, Object> map = new HashMap<>();

        map.put("log", LogUtil.entityList(dept));
        map.put("dept", dept);
        map.put("createBy", createBy);
        map.put("updateBy", updateBy);
        return map;
    }

    /**
     * 设置一条或者多条数据的状态
     */
    @RequestMapping("/status/{param}")
    @RequiresPermissions("/dept/status")
    @ResponseBody
    @ActionLog(name = "部门状态", action = StatusAction.class)
    public ResultVo status(
            @PathVariable("param") String param,
            @RequestParam(value = "ids", required = false) List<Long> idList){
        try {
            // 获取状态StatusEnum对象
            StatusEnum statusEnum = StatusEnum.valueOf(param.toUpperCase());
            // 更新状态
            Integer count = deptService.updateStatus(statusEnum,idList);
            if (count > 0){
                return ResultVoUtil.success(statusEnum.getMessage()+"成功");
            }else{
                return ResultVoUtil.error(statusEnum.getMessage()+"失败，请重新操作");
            }
        } catch (IllegalArgumentException e){
            throw new ResultException(ResultEnum.STATUS_ERROR);
        }
    }

}
