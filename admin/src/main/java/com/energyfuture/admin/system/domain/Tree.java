package com.energyfuture.admin.system.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Tree {
	private String inName;//定义入参显示名称，在实际应用中根据表查询字段定义
	private String inCode;//定义入参主键
	private String inParent;//定义入参父级id
	private String inJb;//定义入参级别
	private String inOtherData;//自定义入参名称
	private String outName;//定义输出的名称
	private String outCode;//定义输出主键名称
	private String outChild;//定义输出子集的名称
	private String outJb;//定义出参级别
	private String outOtherData;//自定义输出参数名称
}
