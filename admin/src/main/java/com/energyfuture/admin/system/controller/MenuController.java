package com.energyfuture.admin.system.controller;

import com.energyfuture.admin.core.enums.ResultEnum;
import com.energyfuture.admin.core.enums.StatusEnum;
import com.energyfuture.admin.core.exception.ResultException;
import com.energyfuture.admin.core.log.action.SaveAction;
import com.energyfuture.admin.core.log.action.StatusAction;
import com.energyfuture.admin.core.log.annotation.ActionLog;
import com.energyfuture.admin.core.utils.LogUtil;
import com.energyfuture.admin.core.utils.PackageTree;
import com.energyfuture.admin.core.web.TimoExample;
import com.energyfuture.admin.system.domain.Menu;
import com.energyfuture.admin.system.domain.User;
import com.energyfuture.admin.system.service.MenuService;
import com.energyfuture.admin.system.validator.MenuForm;
import com.energyfuture.core.utils.FormBeanUtil;
import com.energyfuture.core.utils.ResultVoUtil;
import com.energyfuture.core.vo.ResultVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Sort;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 小懒虫
 * @date 2018/8/14
 */
@RestController
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    private MenuService menuService;

    /**
     * 菜单数据列表
     */
    @GetMapping("/treeList")
    @RequiresPermissions("/menu/index")
    @ResponseBody
    public ResultVo treeList(Menu menu) {
        // 创建匹配器，进行动态查询匹配
        ExampleMatcher matcher = ExampleMatcher.matching().
                withMatcher("title", match -> match.contains());

        // 获取用户列表
        Example<Menu> example = TimoExample.of(menu, matcher);
        Sort sort = new Sort(Sort.Direction.ASC, "sort");
        List<Menu> list = menuService.getList(example, sort);
//        list.forEach(editMenu -> {
//            String type = String.valueOf(editMenu.getType());
//            editMenu.setRemark(DictUtil.keyValue("MENU_TYPE", type));
//        });

        List<Map<String, Object>> result = new ArrayList<>();
        //实体转换为MAP
        for (Object ob : list) {
            try {
                result.add(PackageTree.beanToMap(ob));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //组装前台树结构
        List<Map<String,Object>> treeData = PackageTree.pkTree(result,"title",
                "id","pid","sort","status,pid",
                "title","id","children","sort","status,pid");
        return ResultVoUtil.success(treeData);
    }

    /**
     * 获取排序菜单列表
     */
    @GetMapping("/sortList/{pid}/{notId}")
    @RequiresPermissions({"/menu/add", "/menu/edit"})
    @ResponseBody
    public List<Map<String, Object>> sortList(
            @PathVariable(value = "pid", required = false) Long pid,
            @PathVariable(value = "notId", required = false) Long notId){
        // 本级排序菜单列表
        notId = notId != null ? notId : (long) 0;
        List<Menu> levelMenu = menuService.getPid(pid, notId);
        //添加首位选项
        Menu menu = new Menu();
        menu.setId((long) 0);
        menu.setTitle("首位");
        levelMenu.add(0, menu);

        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = 0; i < levelMenu.size(); i++) {
            Map<String, Object> m = new HashMap<>();
            m.put("id", i);
            m.put("label", levelMenu.get(i).getTitle());
            m.put("value", levelMenu.get(i).getTitle());
            list.add(m);
        }
        return list;
    }

    /**
     * 跳转到添加页面
     */
    @GetMapping("/add")
    @RequiresPermissions("/menu/add")
    public List<Map<String, Object>> toAdd() {
        List<Map<String, Object>> tree = new ArrayList<>();
        Menu menu = new Menu();
        List<Map<String, Object>> list = (List<Map<String, Object>>)treeList(menu).getData();

        Map<String, Object> m = new HashMap<>();
        m.put("id", 0);
        m.put("title", "顶级");
        m.put("children", list);
        m.put("expand", true);
        tree.add(m);

        return tree;
    }

    /**
     * 跳转到编辑页面
     */
    @GetMapping("/edit/{id}")
    @RequiresPermissions("/menu/edit")
    @ResponseBody
    public Map<String, Object> toEdit(@PathVariable("id") Long id) {
        Menu menu = menuService.getId(id);
        Menu pMenu = menuService.getId(menu.getPid());
        Menu mu = new Menu();
        List<Map<String, Object>> list = (List<Map<String, Object>>)treeList(mu).getData();
        Map<String, Object> map = new HashMap<>();

        if (pMenu == null) {
            Menu newMenu = new Menu();
            newMenu.setId((long) 0);
            newMenu.setTitle("顶级菜单");
            pMenu = newMenu;
        }

        List<Map<String, Object>> tree = new ArrayList<>();
        Map<String, Object> m = new HashMap<>();
        m.put("id", 0);
        m.put("title", "顶级");
        m.put("children", list);
        m.put("expand", true);
        tree.add(m);

        map.put("list", tree);
        map.put("menu", menu);
        map.put("pMenu", pMenu);
        return map;
    }

    /**
     * 保存添加/修改的数据
     *
     * @param menuForm 表单验证对象
     */
    @PostMapping("/save")
    @RequiresPermissions({"/menu/add", "/menu/edit"})
    @ResponseBody
    @ActionLog(name = "菜单管理", message = "菜单：${title}", action = SaveAction.class)
    public ResultVo save(@Validated MenuForm menuForm) {
         if (menuForm.getId() == null) {
            // 排序为空时，添加到最后
            if(menuForm.getSort() == null){
                Integer sortMax = menuService.getSortMax(menuForm.getPid());
                menuForm.setSort(sortMax !=null ? sortMax - 1 : 0);
            }

            // 添加全部上级序号
            if (menuForm.getPid() != 0) {
                Menu pMenu = menuService.getId(menuForm.getPid());
                menuForm.setPids(pMenu.getPids() + ",[" + menuForm.getPid() + "]");
            } else {
                menuForm.setPids("[0]");
            }
        }

        // 将验证的数据复制给实体类
        Menu menu = new Menu();
        if (menuForm.getId() != null) {
            menu = menuService.getId(menuForm.getId());
            menuForm.setPids(menu.getPids());
        }
        FormBeanUtil.copyProperties(menuForm, menu);

        // 排序功能
        Integer sort = menuForm.getSort();
        Long notId = menu.getId() != null ? menu.getId() : 0;
        List<Menu> levelMenu = menuService.getPid(menuForm.getPid(), notId);
        levelMenu.add(sort, menu);
        for (int i = 1; i <= levelMenu.size(); i++) {
            levelMenu.get(i - 1).setSort(i);
        }

        // 保存数据
        menuService.save(levelMenu);
        return ResultVoUtil.SAVE_SUCCESS;
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    @RequiresPermissions("/menu/detail")
    @ResponseBody
    public Map<String, Object> toDetail(@PathVariable("id") Long id) {
        Menu menu = menuService.getId(id);
        User createBy = menu.getCreateBy();
        User updateBy = menu.getUpdateBy();

        Map<String, Object> map = new HashMap<>();
        map.put("log", LogUtil.entityList(menu));
        map.put("menu", menu);
        map.put("createBy", createBy);
        map.put("updateBy", updateBy);
        return map;
    }

    /**
     * 设置一条或者多条数据的状态
     */
    @RequestMapping("/status/{param}")
    @RequiresPermissions("/menu/status")
    @ResponseBody
    @ActionLog(name = "菜单状态", action = StatusAction.class)
    public ResultVo status(
            @PathVariable("param") String param,
            @RequestParam(value = "ids", required = false) List<Long> idList) {
        try {
            // 获取状态StatusEnum对象
            StatusEnum statusEnum = StatusEnum.valueOf(param.toUpperCase());
            // 更新状态
            Integer count = menuService.updateStatus(statusEnum, idList);
            if (count > 0) {
                return ResultVoUtil.success(statusEnum.getMessage() + "成功");
            } else {
                return ResultVoUtil.error(statusEnum.getMessage() + "失败，请重新操作");
            }
        } catch (IllegalArgumentException e) {
            throw new ResultException(ResultEnum.STATUS_ERROR);
        }
    }


}
