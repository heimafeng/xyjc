package com.energyfuture.admin.system.controller;

import com.energyfuture.admin.core.constant.AdminConst;
import com.energyfuture.admin.core.enums.ResultEnum;
import com.energyfuture.admin.core.enums.StatusEnum;
import com.energyfuture.admin.core.enums.UserIsRoleEnum;
import com.energyfuture.admin.core.excel.ExcelUtil;
import com.energyfuture.admin.core.exception.ResultException;
import com.energyfuture.admin.core.log.action.StatusAction;
import com.energyfuture.admin.core.log.action.UserAction;
import com.energyfuture.admin.core.log.annotation.ActionLog;
import com.energyfuture.admin.core.shiro.ShiroUtil;
import com.energyfuture.admin.core.utils.LogUtil;
import com.energyfuture.admin.system.domain.Dept;
import com.energyfuture.admin.system.domain.Role;
import com.energyfuture.admin.system.domain.User;
import com.energyfuture.admin.system.repository.UserRepository;
import com.energyfuture.admin.system.service.DeptService;
import com.energyfuture.admin.system.service.RoleService;
import com.energyfuture.admin.system.service.UserService;
import com.energyfuture.admin.system.validator.UserForm;
import com.energyfuture.core.config.properties.ProjectProperties;
import com.energyfuture.core.utils.FormBeanUtil;
import com.energyfuture.core.utils.ResultVoUtil;
import com.energyfuture.core.utils.SpringContextUtil;
import com.energyfuture.core.vo.ResultVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;


/**
 * @author 小懒虫
 * @date 2018/8/14
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private DeptService deptService;

    /**
     * 列表页面
     */
    @GetMapping("/index")
    @ResponseBody
    public Map<String, Object> index(User user) {

        // 设置部门动态查询
        Dept dept = user.getDept();
        List<Long> deptIn = null;
        if(dept != null){
            deptIn = new ArrayList<>();
            deptIn.add(dept.getId());
            List<Dept> deptList = deptService.getPidsLike(dept.getId());
            for (Dept item : deptList) {
                deptIn.add(item.getId());
            }
        }

        // 获取用户列表
        Page<User> list = userService.getPageList(user, deptIn);
        Map<String, Object> maps = new HashMap<>();

        // 封装数据
        maps.put("list", list.getContent());
        maps.put("page", list);
        maps.put("dept", dept);

        return  maps;
    }

    /**
     * 保存添加/修改的数据
     *
     * @param userForm 表单验证对象
     */
    @PostMapping("/save")
    @RequiresPermissions({"/user/add", "/user/edit"})
    @ResponseBody
    @ActionLog(key = UserAction.USER_SAVE, action = UserAction.class)
    public ResultVo  save(@Validated UserForm userForm) {

        // 验证数据是否合格
        if (userForm.getId() == null) {
            // 判断账号是否重复
            User soleUser = userService.getByName(userForm.getUsername(), StatusEnum.FREEZED.getCode());
            if (soleUser != null) {
                throw new ResultException(ResultEnum.USER_EXIST);
            }

            // 判断密码是否为空
            if (userForm.getPassword().isEmpty() || "".equals(userForm.getPassword().trim())) {
                throw new ResultException(ResultEnum.USER_PWD_NULL);
            }

            // 判断两次密码是否一致
            if (!userForm.getPassword().equals(userForm.getConfirm())) {
                throw new ResultException(ResultEnum.USER_INEQUALITY);
            }
            // 对密码进行加密
            String salt = ShiroUtil.getRandomSalt();
            String encrypt = ShiroUtil.encrypt(userForm.getPassword(), salt);
            userForm.setPassword(encrypt);
            userForm.setSalt(salt);
        }

        // 将验证的数据复制给实体类
        User user = new User();
        if (userForm.getId() != null) {
            // 不允许操作超级管理员数据
            if (userForm.getId().equals(AdminConst.ADMIN_ID) &&
                    !ShiroUtil.getSubject().getId().equals(AdminConst.ADMIN_ID)) {
                throw new ResultException(ResultEnum.NO_ADMIN_AUTH);
            }
            // 判断账号是否重复
            user = userService.getByName(userForm.getUsername(), StatusEnum.FREEZED.getCode());
            if (user != null && !user.getId().equals(userForm.getId())) {
                throw new ResultException(ResultEnum.USER_EXIST);
            }
            String[] ignore = {"password", "salt", "picture", "roles", "isRole"};
            FormBeanUtil.copyProperties(userForm, user, ignore);
        } else {
            FormBeanUtil.copyProperties(userForm, user);
        }

        // 保存数据
        User save = userService.save(user);
        // 处理由于Jpa延迟加载特性导致注入entity实体对象失败的问题。
        userForm.setEntity(save);
        return ResultVoUtil.SAVE_SUCCESS;
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    @RequiresPermissions("/user/detail")
    @ResponseBody
    public Map<String, Object> toDetail(@PathVariable("id") Long id) {
        User user = userService.getId(id);
        Map<String, Object> map = new HashMap<>();
        map.put("user", user);
        map.put("log", LogUtil.entityList(user));
        return map;
    }

    /**
     * 修改密码
     */
    @PostMapping("/pwd")
    @ResponseBody
    @ActionLog(key = UserAction.EDIT_PWD, action = UserAction.class)
    public ResultVo editPassword(String password, String confirm,
                                 @RequestParam(value = "ids", required = false) List<Long> idList) {

        // 判断密码是否为空
        if (password.isEmpty() || "".equals(password.trim())) {
            throw new ResultException(ResultEnum.USER_PWD_NULL);
        }

        // 判断两次密码是否一致
        if (!password.equals(confirm)) {
            throw new ResultException(ResultEnum.USER_INEQUALITY);
        }

        // 不允许操作超级管理员数据
        if (idList.contains(AdminConst.ADMIN_ID) &&
                !ShiroUtil.getSubject().getId().equals(AdminConst.ADMIN_ID)) {
            throw new ResultException(ResultEnum.NO_ADMIN_AUTH);
        }

        // 修改密码，对密码进行加密
        List<User> userList = userService.getIdList(idList);
        userList.forEach(user -> {
            String salt = ShiroUtil.getRandomSalt();
            String encrypt = ShiroUtil.encrypt(password, salt);
            user.setPassword(encrypt);
            user.setSalt(salt);
        });

        // 保存数据
        userService.save(userList);
        return ResultVoUtil.success("修改成功");
    }

    /**
     * 角色分配
     */
    @GetMapping("/role")
    @ResponseBody
    @RequiresPermissions("/user/role")
    public Map<String, Object> toRole(@RequestParam(value = "ids") Long id) {
        // 获取指定用户角色列表
        User user = userService.getId(id);
        Set<Role> authRoles = user.getRoles();
        // 获取全部菜单列表
        Sort sort = new Sort(Sort.Direction.ASC, "createDate");
        List<Role> list = roleService.getList(sort);
        // 融合两项数据
        list.forEach(role -> {
            if (authRoles.contains(role)) {
                role.setSelected(true);
            } else {
                role.setSelected(false);
            }
        });
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("list", list);
        return map;
    }

    /**
     * 保存角色分配信息
     */
    @PostMapping("/role")
    @RequiresPermissions("/user/role")
    @ResponseBody
    @ActionLog(key = UserAction.EDIT_ROLE, action = UserAction.class)
    public ResultVo auth(
            @RequestParam(value = "id", required = true) Long id,
            @RequestParam(value = "roleId", required = false) List<Long> roleIds) {

        // 不允许操作超级管理员数据
        if (id.equals(AdminConst.ADMIN_ID) &&
                !ShiroUtil.getSubject().getId().equals(AdminConst.ADMIN_ID)) {
            throw new ResultException(ResultEnum.NO_ADMIN_AUTH);
        }

        // 将查询的数据关联起来
        User user = userService.getId(id);
        List<Role> roleList = null;
        if (roleIds != null) {
            roleList = roleService.getIdList(roleIds);
            user.setRoles(new HashSet<>(roleList));
            user.setIsRole(UserIsRoleEnum.YES.getCode());
        } else {
            user.setRoles(null);
            user.setIsRole(UserIsRoleEnum.NO.getCode());
        }
        // 保存数据
        userService.save(user);
        return ResultVoUtil.SAVE_SUCCESS;
    }

    /**
     * 获取用户头像
     */
    @GetMapping("/picture")
    public void picture(String p, HttpServletResponse response) throws IOException {
        String defaultPath = "/images/user-picture.jpg";
        if (!(StringUtils.isEmpty(p) || p.equals(defaultPath))) {
            ProjectProperties properties = SpringContextUtil.getBean(ProjectProperties.class);
            String fuPath = properties.getFileUploadPath();
            String spPath = properties.getStaticPathPattern().replace("*", "");
            String s = fuPath + p.replace(spPath, "");
            File file = new File(fuPath + p.replace(spPath, ""));
            if(file.exists()){
                FileCopyUtils.copy(new FileInputStream(file), response.getOutputStream());
                return;
            }
        }
        Resource resource = new ClassPathResource("static" + defaultPath);
        FileCopyUtils.copy(resource.getInputStream(), response.getOutputStream());
    }

    /**
     * 导出用户数据
     */
    @GetMapping("/export")
    @ResponseBody
    public void exportExcel(){
        UserRepository userRepository = SpringContextUtil.getBean(UserRepository.class);
        ExcelUtil.exportExcel(User.class, userRepository.findAll());
    }

    /**
     * 设置一条或者多条数据的状态
     */
    @RequestMapping("/status/{param}")
    @RequiresPermissions("/user/status")
    @ResponseBody
    @ActionLog(name = "用户状态", action = StatusAction.class)
    public ResultVo delete(
             @PathVariable("param") String param,
             @RequestParam(value = "ids", required = false) List<Long> idList
            ) {
        // 不能修改超级管理员状态
        if (idList.contains(AdminConst.ADMIN_ID)) {
            throw new ResultException(ResultEnum.NO_ADMIN_STATUS);
        }
        try {
            // 获取状态StatusEnum对象
            StatusEnum statusEnum = StatusEnum.valueOf(param.toUpperCase());
            // 更新状态
            Integer count = userService.updateStatus(statusEnum, idList);
            if (count > 0) {
                String str = org.apache.commons.lang.StringUtils.join(idList.toArray(), ",");
                return ResultVoUtil.success(statusEnum.getMessage() + "成功",str);
            } else {
                return ResultVoUtil.error(statusEnum.getMessage() + "失败，请重新操作");
            }
        } catch (IllegalArgumentException e) {
            throw new ResultException(ResultEnum.STATUS_ERROR);
        }
    }

//    @GetMapping("/exported")
//    @ResponseBody
//    public  List<Map<String,Object>> getVoteList() {
//        List<Object> userList = userService.findVoteList();
//        List<Map<String,Object>> list = new ArrayList<>();
//
//        for (Object row : userList) {
//            Object[] rowArray = (Object[]) row;
//            Map<String, Object> mapArr = new HashMap<String, Object>();
//            mapArr.put("uid", rowArray[0]);
//            mapArr.put("nickname", rowArray[1]);
//            mapArr.put("username", rowArray[2]);
//            mapArr.put("sex", rowArray[4]);
//            mapArr.put("email", rowArray[5]);
//            mapArr.put("phone", rowArray[6]);
//            mapArr.put("is_role", rowArray[7]);
//            mapArr.put("remark", rowArray[8]);
//            mapArr.put("create_date", rowArray[9]);
//            mapArr.put("status", rowArray[10]);
//            mapArr.put("title", rowArray[11]);
//            mapArr.put("pid", rowArray[12]);
//            list.add(mapArr);
//        }
//        return list;
//    }
}
